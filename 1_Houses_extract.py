import os
import json
import requests
gw = 3 # Change the Gameweek number here


## BWB
# Config

output_folder = './BWB'  # Name of output folder
team_ids = [str(197062),str(1298),str(10445),str(902),str(170413),str(198257),str(3889)]  # Teams whose data we want (strings)
league_ids = [str(229619)]  # Leagues whose data we want (strings)
base_endpoint = 'https://fantasy.premierleague.com/drf'

def save_data(filepath, endpoint):
    r = requests.get(base_endpoint + endpoint)

    dirs = filepath.rsplit('/', 1)[0]

    if not os.path.exists(dirs):
        os.makedirs(dirs)

    with open(filepath, 'w') as outfile:
        json_string = json.loads(r.text)
        json.dump(json_string, outfile, indent=4)
        print('saved ' + filepath)


# General endpoints
sources = [
    {
      'endpoint': '/bootstrap-static',
      'filepath': '/general/main'
    },
    {
      'endpoint': '/teams',
      'filepath': '/general/teams'
    },
    {
      'endpoint': '/elements',
      'filepath': '/general/players'
    },
    {
      'endpoint': '/events',
      'filepath': '/general/gameweeks'
    },
    {
      'endpoint': '/game-settings',
      'filepath': '/general/settings'
    }
]

# Team stats
for t_id in team_ids:
    sources.append({
        'endpoint': '/entry/' + t_id,
        'filepath': '/teams/' + t_id + '/main'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/history',
        'filepath': '/teams/' + t_id + '/history'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/transfers',
        'filepath': '/teams/' + t_id + '/transfers'
    })

    for event_id in range(gw,gw+1):
        e_id = str(event_id)

        sources.append({
            'endpoint': '/entry/' + t_id + '/event/' + e_id + '/picks',
            'filepath': '/teams/' + t_id + '/picks/gameweek-' + e_id
        })

# Gameweek stats
for event_id in range(gw,gw+1):
    e_id = str(event_id)

    sources.append({
        'endpoint': '/event/' + e_id + '/live',
        'filepath': '/gameweeks/gameweek-' + e_id
    })

# League stats
for l_id in league_ids:
    sources.append({
        'endpoint': '/leagues-classic-standings/' + l_id,
        'filepath': '/leagues/' + l_id + '-standings'
    })

for source in sources:
    filepath = output_folder + source['filepath'] + '.json'
    save_data(filepath, source['endpoint'])

### WhiteWalkers
output_folder = './WW'  # Name of output folder
team_ids = [str(19781),str(45527),str(137874),str(6143),str(206513),str(198275),str(308784)]  # Teams whose data we want (strings)
league_ids = []  # Leagues whose data we want (strings)
base_endpoint = 'https://fantasy.premierleague.com/drf'

def save_data(filepath, endpoint):
    r = requests.get(base_endpoint + endpoint)

    dirs = filepath.rsplit('/', 1)[0]

    if not os.path.exists(dirs):
        os.makedirs(dirs)

    with open(filepath, 'w') as outfile:
        json_string = json.loads(r.text)
        json.dump(json_string, outfile, indent=4)
        print('saved ' + filepath)


# General endpoints
sources = [
    {
      'endpoint': '/bootstrap-static',
      'filepath': '/general/main'
    },
    {
      'endpoint': '/teams',
      'filepath': '/general/teams'
    },
    {
      'endpoint': '/elements',
      'filepath': '/general/players'
    },
    {
      'endpoint': '/events',
      'filepath': '/general/gameweeks'
    },
    {
      'endpoint': '/game-settings',
      'filepath': '/general/settings'
    }
]

# Team stats
for t_id in team_ids:
    sources.append({
        'endpoint': '/entry/' + t_id,
        'filepath': '/teams/' + t_id + '/main'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/history',
        'filepath': '/teams/' + t_id + '/history'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/transfers',
        'filepath': '/teams/' + t_id + '/transfers'
    })

    for event_id in range(gw,gw+1):
        e_id = str(event_id)

        sources.append({
            'endpoint': '/entry/' + t_id + '/event/' + e_id + '/picks',
            'filepath': '/teams/' + t_id + '/picks/gameweek-' + e_id
        })

# Gameweek stats
for event_id in range(gw,gw+1):
    e_id = str(event_id)

    sources.append({
        'endpoint': '/event/' + e_id + '/live',
        'filepath': '/gameweeks/gameweek-' + e_id
    })

# League stats
for l_id in league_ids:
    sources.append({
        'endpoint': '/leagues-classic-standings/' + l_id,
        'filepath': '/leagues/' + l_id + '-standings'
    })

for source in sources:
    filepath = output_folder + source['filepath'] + '.json'
    save_data(filepath, source['endpoint'])

### Dothrakis
output_folder = './DO'  # Name of output folder
team_ids = [str(7209),str(5930),str(7193),str(240501),str(1889),str(220662),str(256831)]  # Teams whose data we want (strings)
league_ids = []  # Leagues whose data we want (strings)
base_endpoint = 'https://fantasy.premierleague.com/drf'

def save_data(filepath, endpoint):
    r = requests.get(base_endpoint + endpoint)

    dirs = filepath.rsplit('/', 1)[0]

    if not os.path.exists(dirs):
        os.makedirs(dirs)

    with open(filepath, 'w') as outfile:
        json_string = json.loads(r.text)
        json.dump(json_string, outfile, indent=4)
        print('saved ' + filepath)


# General endpoints
sources = [
    {
      'endpoint': '/bootstrap-static',
      'filepath': '/general/main'
    },
    {
      'endpoint': '/teams',
      'filepath': '/general/teams'
    },
    {
      'endpoint': '/elements',
      'filepath': '/general/players'
    },
    {
      'endpoint': '/events',
      'filepath': '/general/gameweeks'
    },
    {
      'endpoint': '/game-settings',
      'filepath': '/general/settings'
    }
]

# Team stats
for t_id in team_ids:
    sources.append({
        'endpoint': '/entry/' + t_id,
        'filepath': '/teams/' + t_id + '/main'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/history',
        'filepath': '/teams/' + t_id + '/history'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/transfers',
        'filepath': '/teams/' + t_id + '/transfers'
    })

    for event_id in range(gw,gw+1):
        e_id = str(event_id)

        sources.append({
            'endpoint': '/entry/' + t_id + '/event/' + e_id + '/picks',
            'filepath': '/teams/' + t_id + '/picks/gameweek-' + e_id
        })

# Gameweek stats
for event_id in range(gw,gw+1):
    e_id = str(event_id)

    sources.append({
        'endpoint': '/event/' + e_id + '/live',
        'filepath': '/gameweeks/gameweek-' + e_id
    })

# League stats
for l_id in league_ids:
    sources.append({
        'endpoint': '/leagues-classic-standings/' + l_id,
        'filepath': '/leagues/' + l_id + '-standings'
    })

for source in sources:
    filepath = output_folder + source['filepath'] + '.json'
    save_data(filepath, source['endpoint'])


### Valeryans
output_folder = './VAL'  # Name of output folder
team_ids = [str(25581),str(135879),str(29505),str(72066),str(204300),str(1176),str(159315)]  # Teams whose data we want (strings)
league_ids = []  # Leagues whose data we want (strings)
base_endpoint = 'https://fantasy.premierleague.com/drf'

def save_data(filepath, endpoint):
    r = requests.get(base_endpoint + endpoint)

    dirs = filepath.rsplit('/', 1)[0]

    if not os.path.exists(dirs):
        os.makedirs(dirs)

    with open(filepath, 'w') as outfile:
        json_string = json.loads(r.text)
        json.dump(json_string, outfile, indent=4)
        print('saved ' + filepath)


# General endpoints
sources = [
    {
      'endpoint': '/bootstrap-static',
      'filepath': '/general/main'
    },
    {
      'endpoint': '/teams',
      'filepath': '/general/teams'
    },
    {
      'endpoint': '/elements',
      'filepath': '/general/players'
    },
    {
      'endpoint': '/events',
      'filepath': '/general/gameweeks'
    },
    {
      'endpoint': '/game-settings',
      'filepath': '/general/settings'
    }
]

# Team stats
for t_id in team_ids:
    sources.append({
        'endpoint': '/entry/' + t_id,
        'filepath': '/teams/' + t_id + '/main'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/history',
        'filepath': '/teams/' + t_id + '/history'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/transfers',
        'filepath': '/teams/' + t_id + '/transfers'
    })

    for event_id in range(gw,gw+1):
        e_id = str(event_id)

        sources.append({
            'endpoint': '/entry/' + t_id + '/event/' + e_id + '/picks',
            'filepath': '/teams/' + t_id + '/picks/gameweek-' + e_id
        })

# Gameweek stats
for event_id in range(gw,gw+1):
    e_id = str(event_id)

    sources.append({
        'endpoint': '/event/' + e_id + '/live',
        'filepath': '/gameweeks/gameweek-' + e_id
    })

# League stats
for l_id in league_ids:
    sources.append({
        'endpoint': '/leagues-classic-standings/' + l_id,
        'filepath': '/leagues/' + l_id + '-standings'
    })

for source in sources:
    filepath = output_folder + source['filepath'] + '.json'
    save_data(filepath, source['endpoint'])


## TARGERYANS
output_folder = './TAR'  # Name of output folder
team_ids = [str(5391),str(647),str(560),str(214837),str(138178),str(2831),str(186003)]  # Teams whose data we want (strings)
league_ids = []  # Leagues whose data we want (strings)
base_endpoint = 'https://fantasy.premierleague.com/drf'

def save_data(filepath, endpoint):
    r = requests.get(base_endpoint + endpoint)

    dirs = filepath.rsplit('/', 1)[0]

    if not os.path.exists(dirs):
        os.makedirs(dirs)

    with open(filepath, 'w') as outfile:
        json_string = json.loads(r.text)
        json.dump(json_string, outfile, indent=4)
        print('saved ' + filepath)


# General endpoints
sources = [
    {
      'endpoint': '/bootstrap-static',
      'filepath': '/general/main'
    },
    {
      'endpoint': '/teams',
      'filepath': '/general/teams'
    },
    {
      'endpoint': '/elements',
      'filepath': '/general/players'
    },
    {
      'endpoint': '/events',
      'filepath': '/general/gameweeks'
    },
    {
      'endpoint': '/game-settings',
      'filepath': '/general/settings'
    }
]

# Team stats
for t_id in team_ids:
    sources.append({
        'endpoint': '/entry/' + t_id,
        'filepath': '/teams/' + t_id + '/main'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/history',
        'filepath': '/teams/' + t_id + '/history'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/transfers',
        'filepath': '/teams/' + t_id + '/transfers'
    })

    for event_id in range(gw,gw+1):
        e_id = str(event_id)

        sources.append({
            'endpoint': '/entry/' + t_id + '/event/' + e_id + '/picks',
            'filepath': '/teams/' + t_id + '/picks/gameweek-' + e_id
        })

# Gameweek stats
for event_id in range(gw,gw+1):
    e_id = str(event_id)

    sources.append({
        'endpoint': '/event/' + e_id + '/live',
        'filepath': '/gameweeks/gameweek-' + e_id
    })

# League stats
for l_id in league_ids:
    sources.append({
        'endpoint': '/leagues-classic-standings/' + l_id,
        'filepath': '/leagues/' + l_id + '-standings'
    })

for source in sources:
    filepath = output_folder + source['filepath'] + '.json'
    save_data(filepath, source['endpoint'])


### NightsWatch

output_folder = './NW'  # Name of output folder
team_ids = [str(588),str(3944),str(1155),str(13195),str(47806),str(47366),str(160803)] # Teams whose data we want (strings)
league_ids = []  # Leagues whose data we want (strings)
base_endpoint = 'https://fantasy.premierleague.com/drf'

def save_data(filepath, endpoint):
    r = requests.get(base_endpoint + endpoint)

    dirs = filepath.rsplit('/', 1)[0]

    if not os.path.exists(dirs):
        os.makedirs(dirs)

    with open(filepath, 'w') as outfile:
        json_string = json.loads(r.text)
        json.dump(json_string, outfile, indent=4)
        print('saved ' + filepath)


# General endpoints
sources = [
    {
      'endpoint': '/bootstrap-static',
      'filepath': '/general/main'
    },
    {
      'endpoint': '/teams',
      'filepath': '/general/teams'
    },
    {
      'endpoint': '/elements',
      'filepath': '/general/players'
    },
    {
      'endpoint': '/events',
      'filepath': '/general/gameweeks'
    },
    {
      'endpoint': '/game-settings',
      'filepath': '/general/settings'
    }
]

# Team stats
for t_id in team_ids:
    sources.append({
        'endpoint': '/entry/' + t_id,
        'filepath': '/teams/' + t_id + '/main'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/history',
        'filepath': '/teams/' + t_id + '/history'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/transfers',
        'filepath': '/teams/' + t_id + '/transfers'
    })

    for event_id in range(gw,gw+1):
        e_id = str(event_id)

        sources.append({
            'endpoint': '/entry/' + t_id + '/event/' + e_id + '/picks',
            'filepath': '/teams/' + t_id + '/picks/gameweek-' + e_id
        })

# Gameweek stats
for event_id in range(gw,gw+1):
    e_id = str(event_id)

    sources.append({
        'endpoint': '/event/' + e_id + '/live',
        'filepath': '/gameweeks/gameweek-' + e_id
    })

# League stats
for l_id in league_ids:
    sources.append({
        'endpoint': '/leagues-classic-standings/' + l_id,
        'filepath': '/leagues/' + l_id + '-standings'
    })

for source in sources:
    filepath = output_folder + source['filepath'] + '.json'
    save_data(filepath, source['endpoint'])


## STARKS

output_folder = './ST'  # Name of output folder
team_ids = [str(1504),str(197016),str(57965),str(1422),str(80062),str(142834),str(164084)]# Teams whose data we want (strings)
league_ids = []  # Leagues whose data we want (strings)
base_endpoint = 'https://fantasy.premierleague.com/drf'

def save_data(filepath, endpoint):
    r = requests.get(base_endpoint + endpoint)

    dirs = filepath.rsplit('/', 1)[0]

    if not os.path.exists(dirs):
        os.makedirs(dirs)

    with open(filepath, 'w') as outfile:
        json_string = json.loads(r.text)
        json.dump(json_string, outfile, indent=4)
        print('saved ' + filepath)


# General endpoints
sources = [
    {
      'endpoint': '/bootstrap-static',
      'filepath': '/general/main'
    },
    {
      'endpoint': '/teams',
      'filepath': '/general/teams'
    },
    {
      'endpoint': '/elements',
      'filepath': '/general/players'
    },
    {
      'endpoint': '/events',
      'filepath': '/general/gameweeks'
    },
    {
      'endpoint': '/game-settings',
      'filepath': '/general/settings'
    }
]

# Team stats
for t_id in team_ids:
    sources.append({
        'endpoint': '/entry/' + t_id,
        'filepath': '/teams/' + t_id + '/main'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/history',
        'filepath': '/teams/' + t_id + '/history'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/transfers',
        'filepath': '/teams/' + t_id + '/transfers'
    })

    for event_id in range(gw,gw+1):
        e_id = str(event_id)

        sources.append({
            'endpoint': '/entry/' + t_id + '/event/' + e_id + '/picks',
            'filepath': '/teams/' + t_id + '/picks/gameweek-' + e_id
        })

# Gameweek stats
for event_id in range(gw,gw+1):
    e_id = str(event_id)

    sources.append({
        'endpoint': '/event/' + e_id + '/live',
        'filepath': '/gameweeks/gameweek-' + e_id
    })

# League stats
for l_id in league_ids:
    sources.append({
        'endpoint': '/leagues-classic-standings/' + l_id,
        'filepath': '/leagues/' + l_id + '-standings'
    })

for source in sources:
    filepath = output_folder + source['filepath'] + '.json'
    save_data(filepath, source['endpoint'])


### FACELESS MEN


output_folder = './FM'  # Name of output folder
team_ids = [str(1170),str(4992),str(209482),str(882),str(203930),str(109456),str(195735)]# Teams whose data we want (strings)
league_ids = []  # Leagues whose data we want (strings)
base_endpoint = 'https://fantasy.premierleague.com/drf'

def save_data(filepath, endpoint):
    r = requests.get(base_endpoint + endpoint)

    dirs = filepath.rsplit('/', 1)[0]

    if not os.path.exists(dirs):
        os.makedirs(dirs)

    with open(filepath, 'w') as outfile:
        json_string = json.loads(r.text)
        json.dump(json_string, outfile, indent=4)
        print('saved ' + filepath)


# General endpoints
sources = [
    {
      'endpoint': '/bootstrap-static',
      'filepath': '/general/main'
    },
    {
      'endpoint': '/teams',
      'filepath': '/general/teams'
    },
    {
      'endpoint': '/elements',
      'filepath': '/general/players'
    },
    {
      'endpoint': '/events',
      'filepath': '/general/gameweeks'
    },
    {
      'endpoint': '/game-settings',
      'filepath': '/general/settings'
    }
]

# Team stats
for t_id in team_ids:
    sources.append({
        'endpoint': '/entry/' + t_id,
        'filepath': '/teams/' + t_id + '/main'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/history',
        'filepath': '/teams/' + t_id + '/history'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/transfers',
        'filepath': '/teams/' + t_id + '/transfers'
    })

    for event_id in range(gw,gw+1):
        e_id = str(event_id)

        sources.append({
            'endpoint': '/entry/' + t_id + '/event/' + e_id + '/picks',
            'filepath': '/teams/' + t_id + '/picks/gameweek-' + e_id
        })

# Gameweek stats
for event_id in range(gw,gw+1):
    e_id = str(event_id)

    sources.append({
        'endpoint': '/event/' + e_id + '/live',
        'filepath': '/gameweeks/gameweek-' + e_id
    })

# League stats
for l_id in league_ids:
    sources.append({
        'endpoint': '/leagues-classic-standings/' + l_id,
        'filepath': '/leagues/' + l_id + '-standings'
    })

for source in sources:
    filepath = output_folder + source['filepath'] + '.json'
    save_data(filepath, source['endpoint'])


### KING SLAYERS

output_folder = './KS'  # Name of output folder
team_ids = [str(50658),str(5954),str(205536),str(203523),str(230582),str(145560),str(92739)]# Teams whose data we want (strings)
league_ids = []  # Leagues whose data we want (strings)
base_endpoint = 'https://fantasy.premierleague.com/drf'

def save_data(filepath, endpoint):
    r = requests.get(base_endpoint + endpoint)

    dirs = filepath.rsplit('/', 1)[0]

    if not os.path.exists(dirs):
        os.makedirs(dirs)

    with open(filepath, 'w') as outfile:
        json_string = json.loads(r.text)
        json.dump(json_string, outfile, indent=4)
        print('saved ' + filepath)


# General endpoints
sources = [
    {
      'endpoint': '/bootstrap-static',
      'filepath': '/general/main'
    },
    {
      'endpoint': '/teams',
      'filepath': '/general/teams'
    },
    {
      'endpoint': '/elements',
      'filepath': '/general/players'
    },
    {
      'endpoint': '/events',
      'filepath': '/general/gameweeks'
    },
    {
      'endpoint': '/game-settings',
      'filepath': '/general/settings'
    }
]

# Team stats
for t_id in team_ids:
    sources.append({
        'endpoint': '/entry/' + t_id,
        'filepath': '/teams/' + t_id + '/main'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/history',
        'filepath': '/teams/' + t_id + '/history'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/transfers',
        'filepath': '/teams/' + t_id + '/transfers'
    })

    for event_id in range(gw,gw+1):
        e_id = str(event_id)

        sources.append({
            'endpoint': '/entry/' + t_id + '/event/' + e_id + '/picks',
            'filepath': '/teams/' + t_id + '/picks/gameweek-' + e_id
        })

# Gameweek stats
for event_id in range(gw,gw+1):
    e_id = str(event_id)

    sources.append({
        'endpoint': '/event/' + e_id + '/live',
        'filepath': '/gameweeks/gameweek-' + e_id
    })

# League stats
for l_id in league_ids:
    sources.append({
        'endpoint': '/leagues-classic-standings/' + l_id,
        'filepath': '/leagues/' + l_id + '-standings'
    })

for source in sources:
    filepath = output_folder + source['filepath'] + '.json'
    save_data(filepath, source['endpoint'])



### LANNISTERS

output_folder = './LAN'  # Name of output folder
team_ids = team_ids = [str(38691),str(27179),str(164846),str(1331),str(336),str(137700),str(199622)]# Teams whose data we want (strings)
league_ids = []  # Leagues whose data we want (strings)
base_endpoint = 'https://fantasy.premierleague.com/drf'

def save_data(filepath, endpoint):
    r = requests.get(base_endpoint + endpoint)

    dirs = filepath.rsplit('/', 1)[0]

    if not os.path.exists(dirs):
        os.makedirs(dirs)

    with open(filepath, 'w') as outfile:
        json_string = json.loads(r.text)
        json.dump(json_string, outfile, indent=4)
        print('saved ' + filepath)


# General endpoints
sources = [
    {
      'endpoint': '/bootstrap-static',
      'filepath': '/general/main'
    },
    {
      'endpoint': '/teams',
      'filepath': '/general/teams'
    },
    {
      'endpoint': '/elements',
      'filepath': '/general/players'
    },
    {
      'endpoint': '/events',
      'filepath': '/general/gameweeks'
    },
    {
      'endpoint': '/game-settings',
      'filepath': '/general/settings'
    }
]

# Team stats
for t_id in team_ids:
    sources.append({
        'endpoint': '/entry/' + t_id,
        'filepath': '/teams/' + t_id + '/main'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/history',
        'filepath': '/teams/' + t_id + '/history'
    })

    sources.append({
        'endpoint': '/entry/' + t_id + '/transfers',
        'filepath': '/teams/' + t_id + '/transfers'
    })

    for event_id in range(gw,gw+1):
        e_id = str(event_id)

        sources.append({
            'endpoint': '/entry/' + t_id + '/event/' + e_id + '/picks',
            'filepath': '/teams/' + t_id + '/picks/gameweek-' + e_id
        })

# Gameweek stats
for event_id in range(gw,gw+1):
    e_id = str(event_id)

    sources.append({
        'endpoint': '/event/' + e_id + '/live',
        'filepath': '/gameweeks/gameweek-' + e_id
    })

# League stats
for l_id in league_ids:
    sources.append({
        'endpoint': '/leagues-classic-standings/' + l_id,
        'filepath': '/leagues/' + l_id + '-standings'
    })

for source in sources:
    filepath = output_folder + source['filepath'] + '.json'
    save_data(filepath, source['endpoint'])



