# -*- coding: utf-8 -*-
import json
import io
import codecs
import pprint
from collections import Counter , OrderedDict

gw = 3;  # Change the Gameweek number here

# Sub and caps for each gw change here
bwb_cap=198257
bwb_sub=3889
tar_cap = 214837
tar_sub = 5391
fm_cap = 209482
fm_sub = 882


#Initialization, dont change
cap_id = 0
bwb_list = []
ww_list=[]
do_list=[]
val_list=[]
tar_list=[]
nw_list=[]
st_list=[]
fm_list=[]
ks_list=[]
lan_list=[]

p_list=[]
with open("/home/sgo/Desktop/BWB/Scripts/BWB_Scripts/BWB/general/players.json") as pl_data: # Reading the PL data
  pl = json.load(pl_data)
  #print(json.dumps(pl, indent=2))

   

# Reading data from each team m

ww= [19781,45527,137874,6143,206513,198275,308784]#whitewalkers

bwb= [10445,902,170413,1298,198257,197062,3889]#brotherhood

do = [7209,5930,7193,240501,1889,220662,256831]#dothrakis

val= [25581,135879,29505,72066,204300,1176,159315]#valyerians

tar= [5391,647,560,214837,138178,2831,186003]#targeryans


nw = [588,3944,1155,13195,47806,47366,160803]#nightswatch

st = [1504,197016,57965,1422,80062,142834,164084]#starks

fm = [1170,4992,209482,882,203930,109456,195735]#facelessmen


ks = [50658,5954,205536,203523,230582,145560,92739]#kingslayers

lan= [38691,27179,164846,1331,336,137700,199622 ]#lannister


###################Brotherhood
for j in range(7):

  if(bwb[j] != bwb_sub):
    with open("/home/sgo/Desktop/BWB/Scripts/BWB_Scripts/BWB/teams/"+str(bwb[j])+"/picks/gameweek-"+str(gw)+".json") as json_data:
      p1 = json.load(json_data)
 
    for picks in p1['picks']:
      cap_var = picks['is_captain']
      #print(type(picks['is_captain']))
      if cap_var == True:
        cap_id = picks['element']
      for i in range(len(pl)-1):
        if(picks['position']<12):
          if(pl[i]['id'] == picks['element']):
            #print(pl[i]['web_name'])
            bwb_list.append(pl[i]['web_name'].encode("utf-8"))
          if(pl[i]['id'] == cap_id):
            bwb_list.append(pl[i]['web_name'].encode("utf-8")) 
      cap_id = 0

  print(j)
  if(bwb[j] == bwb_cap):
    with open("/home/sgo/Desktop/BWB/Scripts/BWB_Scripts/BWB/teams/"+str(bwb[j])+"/picks/gameweek-"+str(gw)+".json") as json_data:
      p1 = json.load(json_data)
 
    for picks in p1['picks']:
      cap_var = picks['is_captain']
      #print(type(picks['is_captain']))
      if cap_var == True:
        cap_id = picks['element']
      for i in range(len(pl)-1):
        if(picks['position']<12):
          if(pl[i]['id'] == picks['element']):
            #print(pl[i]['web_name'])
            bwb_list.append(pl[i]['web_name'].encode("utf-8"))
          if(pl[i]['id'] == cap_id):
            bwb_list.append(pl[i]['web_name'].encode("utf-8")) 
      cap_id = 0


   


counts1 = Counter(bwb_list)
m = OrderedDict(counts1.most_common())
m.keys()
print("BWB")
for key, value in m.items():
  print(key, value)


################FAcelessmen

for j in range(7):

  if(fm[j] != fm_sub):
    with open("/home/sgo/Desktop/BWB/Scripts/BWB_Scripts/FM/teams/"+str(fm[j])+"/picks/gameweek-"+str(gw)+".json") as json_data:
      p1 = json.load(json_data)
 
    for picks in p1['picks']:
      cap_var = picks['is_captain']
      #print(type(picks['is_captain']))
      if cap_var == True:
        cap_id = picks['element']
      for i in range(len(pl)-1):
        if(picks['position']<12):
          if(pl[i]['id'] == picks['element']):
            #print(pl[i]['web_name'])
            fm_list.append(pl[i]['web_name'].encode("utf-8"))
          if(pl[i]['id'] == cap_id):
            fm_list.append(pl[i]['web_name'].encode("utf-8")) 
      cap_id = 0

  print(j)
  if(fm[j] == fm_cap):
    with open("/home/sgo/Desktop/BWB/Scripts/BWB_Scripts/FM/teams/"+str(fm[j])+"/picks/gameweek-"+str(gw)+".json") as json_data:
      p1 = json.load(json_data)
 
    for picks in p1['picks']:
      cap_var = picks['is_captain']
      #print(type(picks['is_captain']))
      if cap_var == True:
        cap_id = picks['element']
      for i in range(len(pl)-1):
        if(picks['position']<12):
          if(pl[i]['id'] == picks['element']):
            #print(pl[i]['web_name'])
            fm_list.append(pl[i]['web_name'].encode("utf-8"))
          if(pl[i]['id'] == cap_id):
            fm_list.append(pl[i]['web_name'].encode("utf-8")) 
      cap_id = 0


   


counts1 = Counter(fm_list)
m = OrderedDict(counts1.most_common())
m.keys()
print("FM")
for key, value in m.items():
  print(key, value)


##### TARG


for j in range(7):

  if(tar[j] != tar_sub):
    with open("/home/sgo/Desktop/BWB/Scripts/BWB_Scripts/TAR/teams/"+str(tar[j])+"/picks/gameweek-"+str(gw)+".json") as json_data:
      p1 = json.load(json_data)
 
    for picks in p1['picks']:
      cap_var = picks['is_captain']
      #print(type(picks['is_captain']))
      if cap_var == True:
        cap_id = picks['element']
      for i in range(len(pl)-1):
        if(picks['position']<12):
          if(pl[i]['id'] == picks['element']):
            #print(pl[i]['web_name'])
            tar_list.append(pl[i]['web_name'].encode("utf-8"))
          if(pl[i]['id'] == cap_id):
            tar_list.append(pl[i]['web_name'].encode("utf-8")) 
      cap_id = 0

  print(j)
  if(tar[j] == tar_cap):
    with open("/home/sgo/Desktop/BWB/Scripts/BWB_Scripts/TAR/teams/"+str(tar[j])+"/picks/gameweek-"+str(gw)+".json") as json_data:
      p1 = json.load(json_data)
 
    for picks in p1['picks']:
      cap_var = picks['is_captain']
      #print(type(picks['is_captain']))
      if cap_var == True:
        cap_id = picks['element']
      for i in range(len(pl)-1):
        if(picks['position']<12):
          if(pl[i]['id'] == picks['element']):
            #print(pl[i]['web_name'])
            tar_list.append(pl[i]['web_name'].encode("utf-8"))
          if(pl[i]['id'] == cap_id):
            tar_list.append(pl[i]['web_name'].encode("utf-8")) 
      cap_id = 0


   


counts1 = Counter(tar_list)
m = OrderedDict(counts1.most_common())
m.keys()
print("TAR")
for key, value in m.items():
  print(key, value)





